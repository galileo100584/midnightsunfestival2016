var COUNT_NEWS = 5;

function empty( mixed_var ) {
  return ( mixed_var === "" || mixed_var === 0   || mixed_var === "0" || mixed_var === null  || mixed_var === false );
}

$(document).ready(function(){
	function getFbFeed() {
          FB.api('473936352700988/posts?fields=full_picture,message,created_time&access_token=879631232127181|8400ce74c72a88f83bb08c551f1d615e', function(response) {
            var html = '';
            for (var i = 0; (i < response.data.length) && (i <= COUNT_NEWS); ++i) {
                if ( ("created_time" in response.data[i]) && ("message" in response.data[i]) ) {
                    
                    //Edit by Martin; Made responsive (col-md-6)
                	html += '<div class="fb-post col-md-6">';
                	var formatedDate = $.format.date(response.data[i].created_time, "MMMM dd at h:mmp");
                  	// html += '<h4 class="fb-header">' + formatedDate + '</h4>';
                    
                    //Edit by martin; Cleaner way to get image url
                    var image = typeof response.data[i].full_picture === "undefined" ? "/images/newsfeed.jpg" : response.data[i].full_picture;

                    //Edit by Leo
                    var s1 = "https://www.facebook.com/"
                    var s2 = response.data[i].id;
                    var fbLink = s1.concat(s2);
                    //console.log(fbLink);
                    //"https://www.facebook.com/midnightsunfestival/?fref=ts"
                    //Edit compleate

                    //Edit by Martin; Put image in a div so we can put image as background 
                    html += '<div class="news-wrapper"><div class="img-feed" style="background-image: url(' + image + ')"></div>';

                    //Edit; The message is compossed of a string with newlines (\n\n), let's split it into an array.
                    var message = response.data[i].message.split('\n\n');

                    //Edit by Martin; Might have somthing else then :::title:::, e.g a youtube url, let's find the first row with a ':::'
                    var titleIndex;
                    for(var k = 0; k < message.length; k++) {
                        if(message[k].indexOf(':::') !== -1) {
                            //k is our titleIndex
                            titleIndex = k;
                        }
                    }
                    var title = message[titleIndex]; //Get our title

                    //Edit by Martin; titleIndex + 1 is the start of the rest of the message, we take 3 elements and join together with newlines
                    message = message.slice(titleIndex + 1, 4).join('\n');
//document.write('<a href="' + burl + '">Open here</a>');
                    //Edit by Martin; Added the date and title in separate <span>'s
                  	html += '<span class="feed-date social-header">' + formatedDate + '"</span><span class="orange social-header feed-title">' + title + '</span><p class="feed-message"><span>' +  message  + '</span></p>';

                  	html += '<p class="read-more"><a href="'+fbLink+'" target="_blank" class="pull-right">[read more]</a </p>';
                  	html += '</div></div>';
                }
            }

            $("#fb-feed").html(html);
          });
      }
      /* make the API call */
      setTimeout(function() {
        getFbFeed();
      }, 1000);

});
