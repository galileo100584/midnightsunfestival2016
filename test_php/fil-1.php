<?php

/**
 * Oppgaver før intervjuet
 * 
 * Før intervjuet ber vi deg sette av 1-3 timer til å løse følgende oppgaver på best mulig måte. 
 * Send resulatet til <a.l.lande@admin.uio.no> innen fristen.
 * 
 * 1. Programmet inneholder en bug den er markert med komkmentaren "FIKSMEG". Rett denne feilen.
 *    Lagre resultatet som fil-1.php
 * 
 * 2. Med resultatet etter oppgave 1 gjør endringer i koden slik at den blir enklere å lese og å utvide.
 *    Lagre resultatet som fil-2.php
 * 
 * 3. Med resultatet etter oppgave 2 utvid programmet som spesifisert under.
 *    Lagre resultatet som fil-3.php
 * 
 * Spesifikasjon av oppg. 3:
 *    Det skal være et maksimum antall av hver rett på menyen (feks. 15 Dagens, 2 Vegetar og 5 Halal).
 *    Dersom man ber om en rett som er utsolgt skal man få bekjeden: "Beklager, <navn på rett> er utsolgt for i dag."
 * 
 */
class Cafeteria {
    public $menu = array("Dagens", "Vegetar", "Halal");
    public $prices = array(55.50, 49.50, 59.50);
    public $sum = 0;
    public $totalSum = 0;

    public function sell($item) {
        if ($item == "Dagens") {
            $this->sum = $this->sum + $this->prices[0];
            $this->totalSum = $this->totalSum + $this->prices[0];
            echo "Dagens, værsågod.";
        }
        else if ($item == "Vegetar") {
            $this->sum = $this->sum + $this->prices[1];
            $this->totalSum = $this->totalSum + $this->prices[1];
            echo "Vegetar, værsågod.";
        }
        else if ($item == "Halal") {
            $this->sum = $this->sum + $this->prices[2];
            $this->totalSum = $this->totalSum + $this->prices[2];
            echo "Halal, værsågod.";
        }
        else {
            throw new Exception($item." er ikke på menyen.");
        }
    }

    public function emptyCashRegistry() {
        $this->sum = 0;
        echo "Kassen er tømt. Tyver vær advart!";
    }

    public function sessionCtrl() {
        session_start();
        if ($_SESSION['totalsum']) {
            $this->totalSum = $_SESSION['totalsum']; }
        if ($_SESSION['sum']) {
            $this->sum = $_SESSION['sum'];
        }
    }

    public function __construct() {
        $this->sessionCtrl();
        $inntext = $_REQUEST['intext'];
        $sub = $_REQUEST['submitted'];
        
        if ($sub) {
            if ($inntext != "ikke sulten") {

                $notAQuestion = false;
                if ($inntext == "hvordan går salget?") {
                    $notAQuestion = true;
                    if ($this->totalSum == 0) {
                        echo "Ikke så bra sålangt. Ingen virker å være sultne i dag!";
                    }
		    else if ($this->totalSum > 500) {
                        // FIKSMEG: denne fungerer ikke helt
                        echo "Strålende! Mange sultne studenter i dag.";
                    }

                    else if ($this->totalSum > 0) {
                        echo "Ok. Men kunne vært bedre.";
                    }
                                    }
                if (!$notAQuestion) {
                    try {
                        $this->sell($inntext);
                    }
                    catch (Exception $e) { 
                        echo ("Beklager: ".$e->getMessage());
                    }
                }
                if ($this->sum > 200) {
                    $this->emptyCashRegistry();
                }
                echo "Hva ønsker du i dag? (skriv 'hvordan går salget?' for å småprate, eller 'ikke sulten' for å stoppe)";
                $_SESSION['totalsum'] = $this->totalSum;
                $_SESSION['sum'] = $this->sum;
            }
            else {
                echo "Velkommen tilbake senere!";
            }
            
        }
        else{
            echo "Hva ønsker du i dag? (skriv 'hvordan går salget?' for å småprate, eller 'ikke sulten' for å stoppe)";
        }
    }

}

new Cafeteria;
?>
<html>
    <head>
        <meta charset="utf-8">
    </head>
        
    <body>
        <form>
            <input name="intext" type="text">
            <input type="submit" name="submitted" value="submit">
        </form>
    </body>
</html>

